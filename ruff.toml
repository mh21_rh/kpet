# Disable the preview feature and formatter
preview = false
fix = false
output-format = "concise"

# Exclude a variety of commonly ignored directories.
extend-exclude = [
    "__pypackages__",
    "_build",
    ".bzr",
    ".direnv",
    ".eggs",
    ".git-rewrite",
    ".git",
    ".hg",
    ".ipynb_checkpoints",
    ".mypy_cache",
    ".nox",
    ".pants.d",
    ".pyenv",
    ".pytest_cache",
    ".pytype",
    ".ruff_cache",
    ".svn",
    ".tox",
    ".venv",
    ".vscode",
    "buck-out",
    "build",
    "dist",
    "node_modules",
    "site-packages",
    "venv",
    "migrations",
    ".vscode-oss",
]

line-length = 79
indent-width = 4

# Assume Python 3.10
target-version = "py310"

[lint]
preview = false

# Allow fix for all enabled rules (when `--fix`) is provided.
fixable = ["ALL"]
unfixable = []
extend-fixable = ["B"]
extend-safe-fixes = ["E", "F401"]
extend-unsafe-fixes = []
# Enable rules (https://docs.astral.sh/ruff/rules/)
select = [
    # flake8-bandit (S)
    "S",
    # flake8-boolean-trap (FBT)
    "FBT",
    # flake8-bugbear (B)
    "B",
    # flake8-commas (COM)
    "COM",
    # flake8-comprehensions (C4)
    "C4",
    # flake8-datetimez (DTZ)
    "DTZ",
    # flake8-django (DJ)
    "DJ",
    # flake8-implicit-str-concat (ISC)
    "ISC",
    # flake8-logging-format (G)
    "G",
    # flake8-no-pep420 (INP)
    "INP",
    # flake8-pie (PIE)
    "PIE",
    # flake8-return (RET)
    "RET",
    # flake8-simplify (SIM)
    "SIM",
    # flake8-unused-arguments (ARG)
    "ARG",
    # isort (I)
    "I",
    # mccabe (C90)
    "C90",
    # pep8-naming (N)
    "N",
    # Perflint (PERF)
    "PERF",
    # pycodestyle error (E) and warnings (W)
    "E", "W",
    # pydocstyle (`D`)
    "D",
    # Pyflakes (`F`)
    "F",
    # Pylint (PL)
    "PL",
    # pyupgrade (UP)
    "UP",
    # Ruff-specific rules (RUF)
    "RUF",
    # Multi-line docstrings should have their summary on the second line
    "D213",
]
ignore = [
    # undocumented-param: Do not require documentation for every function parameter.
    "D417",
    # unnecessary-assign: We actually like to assign big statements to variables to return
    "RET504",
    # camelcase-imported-as-acronym: Do not complain about aliases for long CamelCase imports being acronyms
    "N817",
    # Disable rules that might conflict with the formatter
    # missing-trailing-comma: Do not enforce trailing commas
    "COM812",
    # I personally don't care about too many public methods
    "PLR0904",
    # There could be a good reason for no-self-use
    "PLR6301",
    # Multi-line docstrings should have their summary on the second line
    "D212",
    # Checks that we have not yet decided whether we want or not and we disable because they are covering 
    # more than the linters we have used previously.
    # We want to eventually go through these and either enable them and fix all occurences in our projects
    # or move them out of this section with a comment on why we want them ignored.
    "ARG001",  # Unused function argument
    "ARG002",  # Unused method argument
    "ARG003",  # Unused class method
    "ARG004",  # Unused static method argument
    "ARG005",  # Unused lambda argument
    "B005",  # Using .strip() with multi-character strings is misleading
    "B006",  # Do not use mutable data structures for argument defaults
    "B007",  # Loop control variable not used within loop body
    "B008",  # Do not perform function call in argument defaults
    "B009",  # Do not call getattr with a constant attribute value.
    "B010",  # Do not call setattr with a constant attribute value.
    "B011",  # Do not assert False (python -O removes these calls), raise AssertionError()
    "B017",  # Don't use generic exceptions
    "B018",  # Found useless expression. Either assign it to a variable or remove it.
    "B019",  # Use of functools.lru_cache or functools.cache on methods can lead to memory leaks
    "B033",  # Sets should not contain duplicate items
    "B904",  # In the except clause, raise should use the "from" keyword (at least "from None")
    "B905",  # zip() without an explicit strict= parameter
    "C400",  # Unnecessary generator (rewrite using list())
    "C401",  # Unnecessary generator (rewrite using set())
    "C405",  # Unnecessary literal (rewrite as a set literal)
    "C408",  # Unnecessary call (rewrite as a literal)
    "C413",  # Unnecessary call around sorted()
    "C414",  # Unnecessary {inner} call within {outer}()
    "C416",  # Unnecessary {obj_type} comprehension (rewrite using {obj_type}())
    "C901",  # Cyclomatic complexity is over some set maximum
    "COM819",  # Trailing comma prohibited
    "D200",  # One-line docstring should fit on one line
    "D202",  # No blank lines allowed after function docstring
    "D205",  # 1 blank line required between summary line and description
    "D208",  # Docstring is over-indented
    "D209",  # Multi-line docstring closing quotes should be on a separate line
    "D210",  # No whitespaces allowed surrounding docstring text
    "D213",  # Multi-line docstring summary should start at the second line
    "D215",  # Section underline is over-indented
    "D300",  # Use triple double quotes """
    "D301",  # Use r""" if any backslashes in a docstring
    "D403",  # First word of the first line should be capitalized
    "D410",  # Missing blank line after section
    "D411",  # Missing blank line before section
    "D415",  # First line should end with a period, question mark, or exclamation point
    "DJ001",  # Avoid using null=True on string-based fields
    "DTZ001",  # datetime.datetime() called without a tzinfo argument
    "DTZ005",  # datetime.datetime.now() called without a tz argument
    "DTZ006",  # datetime.datetime.fromtimestamp() called without a tz argument
    "DTZ007",  # Naive datetime constructed using datetime.datetime.strptime() without %z
    "E721",  # Use is and is not for type comparisons, or isinstance() for isinstance checks
    "FBT001",  # Boolean-typed positional argument in function definition
    "FBT002",  # Boolean default positional argument in function definition
    "FBT003",  # Boolean positional value in function call
    "I001",  # Import block is un-sorted or un-formatted
    "INP001",  # File is part of an implicit namespace package. Add an __init__.py.
    "ISC002",  # Implicitly concatenated string literals over multiple lines
    "ISC003",  # Explicitly concatenated string should be implicitly concatenated
    "N802",  # Function name should be lowercase
    "N803",  # Argument name should be lowercase
    "N804",  # First argument of a class method should be named cls
    "N805",  # First argument of a method should be named self
    "N806",  # Variable in function should be lowercase
    "N807",  # Function name should not start and end with __
    "N811",  # Constant imported as non-constant
    "N815",  # Variable in class scope should not be mixedCase
    "N816",  # Variable in global scope should not be mixedCase
    "N818",  # Exception name should be named with an Error suffix
    "PERF102",  # When using only the {subset} of a dict use the {subset}() method
    "PERF203",  # try-except within a loop incurs performance overhead
    "PERF401",  # Use a list comprehension to create a transformed list
    "PIE796",  # Enum contains duplicate value
    "PIE804",  # Unnecessary dict kwargs
    "PIE808",  # Unnecessary start argument in range
    "PIE810",  # Call {attr} once with a tuple
    "PLR0402",  # Use from {module} import {name} in lieu of alias
    "PLR0911",  # Too many return statements
    "PLR0912",  # Too many branches
    "PLR0913",  # Too many arguments in function definition
    "PLR0915",  # Too many statements
    "PLR1704",  # Redefining argument with a local name
    "PLR1714",  # Consider merging multiple comparisons
    "PLR2004",  # Magic value used in comparison, consider replacing with a constant variable
    "PLR5501",  # Use elif instead of else then if, to reduce indentation
    "PLW0603",  # Using the global statement to update variable is discouraged
    "PLW1510",  # subprocess.run without explicit check argument
    "PLW2901",  # Outer {outer_kind} variable overwritten by inner {inner_kind} target
    "RET505",  # Unnecessary {branch} after return statement
    "RUF001",  # String contains ambiguous unicode character. 
    "RUF003",  # Comment contains ambiguous unicode character.
    "RUF005",  # Consider {expression} instead of concatenation
    "RUF007",  # Prefer itertools.pairwise() over zip() when iterating over successive pairs
    "RUF008",  # Do not use mutable default values for dataclass attributes
    "RUF009",  # Do not perform function call in dataclass defaults
    "RUF010",  # Use explicit conversion flag
    "RUF012",  # Mutable class attributes should be annotated with typing.ClassVar
    "RUF013",  # PEP 484 prohibits implicit Optional
    "RUF015",  # Prefer next({iterable}) over single element slice
    "RUF100",  # Unused noqa directive
    "S101",  # Use of assert detected
    "S104",  # Possible binding to all interfaces
    "S105",  # Possible hardcoded password assigned
    "S106",  # Possible hardcoded password assigned to argument
    "S108",  # Probable insecure usage of temporary file or directory
    "S301",  # pickle and modules that wrap it can be unsafe when used to deserialize untrusted data
    "S307",  # Use of possibly insecure function; consider using ast.literal_eval
    "S310",  # Audit URL open for permitted schemes. Allowing use of file: or custom schemes is often unexpected.
    "S311",  # Standard pseudo-random generators are not suitable for cryptographic purposes
    "S314",  # Using xml to parse untrusted data is known to be vulnerable to XML attacks
    "S320",  # Using lxml to parse untrusted data is known to be vulnerable to XML attacks
    "S324",  # Probable use of insecure hash functions
    "S506",  # Probable use of unsafe loader with yaml.load.
    "S603",  # subprocess call: check for execution of untrusted input
    "S607",  # Starting a process with a partial executable path
    "S608",  # Possible SQL injection vector through string-based query construction
    "S701",  # Using jinja2 templates with autoescape=False is dangerous and can lead to XSS.
    "SIM102",  # Use a single if statement instead of nested if statements
    "SIM103",  # Return the condition directly
    "SIM105",  # Use contextlib.suppress({exception}) instead of try-except-pass
    "SIM108",  # Use ternary operator instead of if-else-block
    "SIM110",  # Use {replacement} instead of for loop
    "SIM113",  # Use enumerate() for index variable {index} in for loop
    "SIM114",  # Combine if branches using logical or operator
    "SIM117",  # Use a single with statement with multiple contexts instead of nested with statements
    "SIM118",  # Use key {operator} dict instead of key {operator} dict.keys()
    "SIM201",  # Use {left} != {right} instead of not {left} == {right}
    "SIM210",  # Remove unnecessary True if ... else False
    "SIM300",  # Yoda condition detected
    "SIM401",  # Use {contents} instead of an if block
    "SIM910",  # Use {expected} instead of {actual}
    "UP006",  # Use {to} instead of {from} for type annotation
    "UP007",  # Use X | Y for type annotations
    "UP009",  # UTF-8 encoding declaration is unnecessary
    "UP015",  # Unnecessary open mode parameters
    "UP026",  # mock is deprecated, use unittest.mock
    "UP031",  # Use format specifiers instead of percent format
    "UP032",  # Use f-string instead of format call
    "UP033",  # Use @functools.cache instead of @functools.lru_cache(maxsize=None)
    "UP034",  # Avoid extraneous parentheses
    "UP037",  # Remove quotes from type annotation
    "UP038",  # Use X | Y in call instead of (X, Y)
    "UP039",  # Unnecessary parentheses after class definition
]


# Allow unused variables when underscore-prefixed.
dummy-variable-rgx = "^(_+|(_+[a-zA-Z0-9_]*[a-zA-Z0-9]+?))$"

[lint.pep8-naming]
extend-ignore-names = ["id", "i"]

[lint.flake8-boolean-trap]
extend-allowed-calls = ["django.db.models.Value"]

[lint.flake8-builtins]
builtins-ignorelist = ["id"]

[lint.per-file-ignores]
# Unused imports in init files are expected
"__init__.py" = ['F401']
"tests/*" = [
    # We don't care that much about tests docstrings
    "D1",
    # sometimes `import` can't be at the top-level of a file
    "PLC0415",
    # Tests can't have too many public methods.
    "PLR0904",
    # Tests can have unused `self`
    "PLR6301",
    # Test cases are allowed to use `assert`
    "S101",
    # Let tests have too many variables
    "PLR0914",
    # Let tests have too many statements
    "PLR0915",
]

[lint.pydocstyle]
convention = "google"

[lint.isort]
case-sensitive = true
force-single-line = true
force-sort-within-sections = true
relative-imports-order = "closest-to-furthest"
order-by-type = false

[format]

# Enable preview style formatting.
preview = false

# Unlike Black, don't respect trailing commas, they don't deserve it
skip-magic-trailing-comma = false

# Like Black, use double quotes
quote-style = "double"

# Like Black, indent with spaces, rather than tabs.
indent-style = "space"

# Like Black, automatically detect the appropriate line ending.
line-ending = "auto"
