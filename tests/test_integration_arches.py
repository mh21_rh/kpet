# Copyright (c) 2023 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for arches"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_arch_list


class IntegrationArchesTests(IntegrationTests):
    """Tests for arches"""

    def test_list(self):
        """
        Check the "kpet arch list" command works appropriately.
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - a
                  - b
                  - c
                trees:
                  a:
                    arches: a
                  b:
                    arches: b
                  a_and_b:
                    arches: a|b
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  name: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
            """,
            "output.txt.j2": ""
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_arch_list, db_path,
                stdout_equals="a\nb\nc\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-t.*",
                stdout_equals="a\nb\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "--tree=.*",
                stdout_equals="a\nb\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-t.*", ".*",
                stdout_equals="a\nb\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, ".*",
                stdout_equals="a\nb\nc\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-t.*", "",
                stdout_equals=""
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-ta",
                stdout_equals="a\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-ta", "b",
                stdout_equals=""
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-tb",
                stdout_equals="b\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-ta_and_b",
                stdout_equals="a\nb\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-ta_and_b", "a|b",
                stdout_equals="a\nb\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "-ta_and_b", "a",
                stdout_equals="a\n"
            )

            self.assertKpetProduces(
                kpet_arch_list, db_path, "a",
                stdout_equals="a\n"
            )
