# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for test command module"""
import os
import unittest

import mock

from kpet import cmd_test
from kpet import misc


class CmdTestTest(unittest.TestCase):
    """Test cases for test command module."""

    def test_list(self):
        """
        Check the proper exception is raised when action is not found, and if
        an exception is raised when the database directory is invalid.
        """
        dbdir = os.path.join(os.path.dirname(__file__), 'assets/db/general')
        mock_args = mock.Mock()
        mock_args.db = dbdir
        mock_args.domains = None
        mock_args.trees = None
        mock_args.arches = None
        mock_args.components = None
        mock_args.sets = None
        mock_args.tests = ['fs/ext4']
        mock_args.all_sources = None
        mock_args.cookies = None
        mock_args.mboxes = []
        mock_args.file_list = None
        mock_args.output = 'yaml'
        mock_args.files = []
        mock_args.high_cost = 'yes'
        mock_args.targeted = False
        mock_args.triggered = False
        self.assertRaises(misc.ActionNotFound, cmd_test.main, mock_args)
        mock_args.action = 'list'
        with mock.patch('sys.stdout') as mock_stdout:
            cmd_test.main(mock_args)
        expected = [
            mock.call(
                """- name: fs/ext4
  universal_id: test_uid
  origin: foo
  location: /filesystems/xfs/xfstests
  maintainers:
    - name: maint1
      email: maint1@maintainers.org
  host_types:
    - normal
  environment:
    x: a
    y: a
"""),
            mock.call('')
        ]
        self.assertListEqual(
            expected,
            mock_stdout.write.call_args_list,
        )
        mock_args.db = '/notfounddir'
        self.assertRaises(Exception, cmd_test.main, mock_args)
