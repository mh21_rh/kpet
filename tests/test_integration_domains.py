# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for host domains"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate
from tests.test_integration import kpet_run_test_list
from tests.test_integration import kpet_test_list

# It's OK, pylint: disable=too-many-lines,too-many-public-methods

OUTPUT_TXT_J2 = """
    {%- macro host_cond(cond, qualifier, parent_op, op) -%}
        {%- if cond is string -%}
            {{- cond -}}
        {%- elif cond is sequence -%}
            {%- set cp_op = op if cond | length > 1 else parent_op -%}
            {%- set terms = [] -%}
            {%- if cond is mapping -%}
                {%- for k, v in cond.items() -%}
                    {%- if k == "not" -%}
                        {# Yeah, using "!" as parent_op is a hack, sorry #}
                        {%- set term = host_cond(v, qualifier, "!", "&") -%}
                        {%- if term -%}
                            {%- set term = "!" + term -%}
                        {%- endif -%}
                    {%- elif k == "or" -%}
                        {%- set term = host_cond(v, qualifier, cp_op, "|") -%}
                    {%- elif k == "and" -%}
                        {%- set term = host_cond(v, qualifier, cp_op, "&") -%}
                    {%- elif k == "templates" -%}
                        {%- set term = host_cond(v, True, cp_op, "&") -%}
                    {%- elif k == "hostnames" -%}
                        {%- set term = host_cond(v, False, cp_op, "&") -%}
                    {%- else -%}
                        {{- "Invalid host condition key" / 0 -}}
                    {%- endif -%}
                    {%- set _ = terms.append(term) -%}
                {%- endfor -%}
            {%- else -%}
                {%- for v in cond -%}
                    {%- set term = host_cond(v, qualifier, cp_op, "&") -%}
                    {%- set _ = terms.append(term) -%}
                {%- endfor -%}
            {%- endif -%}
            {%- if parent_op is not none and op != parent_op and
                   (parent_op == "!" or op == "|")
                   and cond | length > 1 -%}
                ({{- terms | join(op) -}})
            {%- else -%}
                {{- terms | join(op) -}}
            {%- endif -%}
        {%- else -%}
            {{- "Invalid host condition type" / 0 -}}
        {%- endif -%}
    {%- endmacro -%}
    {%- for scene in SCENES -%}
        {{- "SCENE:" -}}
        {%- for recipeset in scene.recipesets -%}
            {{- "RECIPESET:" -}}
            {%- for host in recipeset -%}
                HOST<{{- host_cond(host.condition, None, None, "&") -}}>:
                {%- for test in host.tests -%}
                    TEST:{{- test.name -}}
                {%- endfor -%}
            {%- endfor -%}
        {%- endfor -%}
    {%- endfor -%}
"""


class IntegrationDomainsTests(IntegrationTests):
    """Integration tests for host domain handling"""

    def test_no_domains_anywhere(self):
        """Check no domains defined, no domains in host types works"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    name: Test
                    host_types: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<>:TEST:Test'
            )

    def test_domains_in_host_types_only(self):
        """Check domains defined only in host types breaks"""
        assets = {
            "index.yaml": """
                host_types:
                    normal:
                        domains: general
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    name: Test
                    host_types: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".* Host type 'normal' specifies "
                                r'"domains" but domains are not defined.*'
            )

    def test_domain_defined_but_not_used(self):
        """
        Check hosts are not instantiated when a domain is defined,
        but not used in host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    name: Test
                    host_types: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
            )

    def test_two_domains_defined_but_not_used(self):
        """
        Check hosts are not instantiated when two domains are defined,
        but not used in host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    name: Test
                    host_types: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
            )

    def test_domain_defined_but_partially_used(self):
        """
        Check specific hosts are not instantiated when a domain is defined,
        but not used in corresponding host types.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                    b: {}
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<>:TEST:Test1'
            )

    def test_domain_everywhere(self):
        """
        Test a domain is defined and used everywhere.
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                    b:
                        domains: general
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<>:TEST:Test1'
                              'RECIPESET:HOST<>:TEST:Test2'
            )

    def test_two_domains_everywhere(self):
        """
        Test two domains are defined and used everywhere.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:HOST<>:TEST:Test1'
                              'RECIPESET:HOST<>:TEST:Test2'
            )

    def test_unknown_domain(self):
        """
        Test host type not matching any known domain is detected
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: special
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r'.* none of the known domain .*'
            )

    def test_one_domain_with_host_requires(self):
        """
        Test one domain with host_requires is handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                        host_requires: A
                host_types:
                    a:
                        domains: general
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<T:A>:TEST:Test'
            )

    def test_one_domain_with_host_requires_one_without(self):
        """
        Test one domain with host_requires, and one without, are handled
        correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<T:x>:TEST:Test'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:HOST<T:x>:TEST:Test1'
                              'RECIPESET:HOST<!T:x>:TEST:Test2'
            )

    def test_two_domains_with_host_requires(self):
        """
        Test two domains, both with host_requires, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                host_types:
                    a:
                        domains: X
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<T:x&!T:y>:TEST:Test'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                host_types:
                    a:
                        domains: X
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:HOST<T:x&!T:y>:TEST:Test1'
                              'RECIPESET:HOST<!T:x&T:y>:TEST:Test2'
            )

    def test_one_domain_with_hostname(self):
        """
        Test one domain with a host type using "hostname" property is handled
        correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<H:a.com>:TEST:Test'
            )

    def test_one_domain_with_hostname_one_without(self):
        """
        Test one domain with a host type using "hostname" property, and one
        without, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:HOST<H:a.com>:TEST:Test'
            )

    def test_two_domains_with_hostnames(self):
        """
        Test two domains, each with a host type using "hostname" property,
        are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                        hostname: b.com
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    name: Test
                    host_types: a
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<H:a.com&!H:b.com>:TEST:Test'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: Y
                        hostname: b.com
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:HOST<H:a.com&!H:b.com>:TEST:Test1'
                              'RECIPESET:HOST<!H:a.com&H:b.com>:TEST:Test2'
            )

    def test_two_domains_with_multiple_hostnames(self):
        """
        Test two domains, each with multiple host types, each using "hostname"
        property, are handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: X
                        hostname: b.com
                    c:
                        domains: Y
                        hostname: c.com
                    d:
                        domains: Y
                        hostname: d.com
                recipesets:
                    rcs1:
                      - a
                      - b
                    rcs2:
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<H:a.com&!H:c.com&!H:d.com>:TEST:Test1'
                              'HOST<H:b.com&!H:c.com&!H:d.com>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        hostname: a.com
                    b:
                        domains: X
                        hostname: b.com
                    c:
                        domains: Y
                        hostname: c.com
                    d:
                        domains: Y
                        hostname: d.com
                recipesets:
                    rcs1:
                      - a
                      - b
                    rcs2:
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
                        test3:
                            name: Test3
                            host_types: c
                        test4:
                            name: Test4
                            host_types: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<H:a.com&!H:c.com&!H:d.com>:TEST:Test1'
                              'HOST<H:b.com&!H:c.com&!H:d.com>:TEST:Test2'
                              'RECIPESET:'
                              'HOST<!H:a.com&!H:b.com&H:c.com>:TEST:Test3'
                              'HOST<!H:a.com&!H:b.com&H:d.com>:TEST:Test4'
            )

    def test_hostname_propagation(self):
        """Check that forced hostnames are propagated appropriately."""
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: true
                        domains:
                            X1:
                                description: Hosts X1
                                host_requires: true
                    Y:
                        description: Hosts Y
                        host_requires: Y
                        domains:
                            Y1:
                                description: Hosts Y1
                                host_requires: Y1
                host_types:
                    x-a:
                        domains: X
                        hostname: x.com
                    x1-a:
                        domains: X1
                        hostname: x1.com
                    y-a:
                        domains: Y
                        hostname: y.com
                    y1-a:
                        domains: Y1
                        hostname: y1.com
                    x-b:
                        domains: X
                    x1-b:
                        domains: X1
                    y-b:
                        domains: Y
                    y1-b:
                        domains: Y1
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test_x_a:
                            name: Test X-A
                            host_types: x-a
                        test_x1_a:
                            name: Test X1-A
                            host_types: x1-a
                        test_y_a:
                            name: Test Y-A
                            host_types: y-a
                        test_y1_a:
                            name: Test Y1-A
                            host_types: y1-a
                        test_x_b:
                            name: Test X-B
                            host_types: x-b
                        test_x1_b:
                            name: Test X1-B
                            host_types: x1-b
                        test_y_b:
                            name: Test Y-B
                            host_types: y-b
                        test_y1_b:
                            name: Test Y1-B
                            host_types: y1-b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<H:x.com&!H:y.com&'
                              '!H:y1.com&!T:Y>:TEST:Test X-A'
                              'RECIPESET:'
                              'HOST<H:x1.com&!H:y.com&'
                              '!H:y1.com&!T:Y>:TEST:Test X1-A'
                              'RECIPESET:'
                              'HOST<!H:x.com&!H:x1.com&'
                              'H:y.com&T:Y>:TEST:Test Y-A'
                              'RECIPESET:'
                              'HOST<!H:x.com&!H:x1.com&'
                              'H:y1.com&T:Y&T:Y1>:TEST:Test Y1-A'
                              'RECIPESET:'
                              'HOST<!H:y.com&!H:y1.com&'
                              '!T:Y>:TEST:Test X-B'
                              'RECIPESET:'
                              'HOST<!H:y.com&!H:y1.com&'
                              '!T:Y>:TEST:Test X1-B'
                              'RECIPESET:'
                              'HOST<!H:x.com&!H:x1.com&'
                              'T:Y>:TEST:Test Y-B'
                              'RECIPESET:'
                              'HOST<!H:x.com&!H:x1.com&'
                              'T:Y&T:Y1>:TEST:Test Y1-B'
            )

    def test_host_type_in_two_domains(self):
        """
        Test a host type in two domains is handled correctly
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:HOST<>:TEST:Test1'
                              'RECIPESET:HOST<>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                    Z:
                        description: Hosts Z
                        host_requires: z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:x&!T:y&!T:z>:TEST:Test1'
                              'RECIPESET:'
                              'HOST<!T:x&!T:y&T:z>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: x
                    Y:
                        description: Hosts Y
                        host_requires: y
                    Z:
                        description: Hosts Z
                        host_requires: z
                host_types:
                    a:
                        domains: X|Y
                        hostname: a.com
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                    rcs2:
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<H:a.com&T:x&!T:y&!T:z>:TEST:Test1'
                              'RECIPESET:'
                              'HOST<!H:a.com&!T:x&!T:y&T:z>:TEST:Test2'
            )

    def test_recipeset_in_multiple_domains(self):
        """
        Test a recipeset with host types in multiple domains is handled
        correctly.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test1'
                              'HOST<>:TEST:Test2'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: X
                    c:
                        domains: Y
                    d:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
                        test3:
                            name: Test3
                            host_types: c
                        test4:
                            name: Test4
                            host_types: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test1'
                              'HOST<>:TEST:Test2'
                              'HOST<>:TEST:Test3'
                              'HOST<>:TEST:Test4'
            )

    def test_domain_selection(self):
        """
        Test domain selection from command line works correctly
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    name: Test
                    host_types: normal
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", ".*", status=1,
                stderr_matching=r'.*no domains specified.*'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                    Z:
                        description: Hosts Z
                host_types:
                    a:
                        domains: X|Y
                    b:
                        domains: X
                    c:
                        domains: Y
                    d:
                        domains: Z
                recipesets:
                    rcs1:
                      - a
                      - b
                      - c
                      - d
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
                        test3:
                            name: Test3
                            host_types: c
                        test4:
                            name: Test4
                            host_types: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", ".*",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test1'
                              'HOST<>:TEST:Test2'
                              'HOST<>:TEST:Test3'
                              'HOST<>:TEST:Test4'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", ".*",
                stdout_equals='Test1\nTest2\nTest3\nTest4\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", ".*",
                stdout_equals='Test1\nTest2\nTest3\nTest4\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test1'
                              'HOST<>:TEST:Test2'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X|Y",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test1'
                              'HOST<>:TEST:Test2'
                              'HOST<>:TEST:Test3'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "X|Y",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "X|Y",
                stdout_equals='Test1\nTest2\nTest3\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "Z",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<>:TEST:Test4'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "Z",
                stdout_equals='Test4\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "Z",
                stdout_equals='Test4\n'
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--domains", "A", status=1,
                stderr_matching=r'.*matches no domains.*'
            )

    def test_architecture_filtering(self):
        """
        Check domains and host types are properly architecture-filtered
        """
        assets = {
            "index.yaml": """
                domains:
                    no_arch:
                        description: Domain without architecture
                    arch_X:
                        description: Domain with architecture X
                        arches: X
                    arch_Y:
                        description: Domain with architecture Y
                        arches: Y
                host_types:
                    all_domains_no_arches:
                        domains: .*
                    all_domains_arch_X:
                        domains: .*
                        supported:
                            arches: X
                    all_domains_arch_Y:
                        domains: .*
                        supported:
                            arches: Y
                    all_domains_all_arches:
                        domains: .*
                        supported:
                            arches: .*

                    no_arch_domain_no_arches:
                        domains: no_arch
                    no_arch_domain_arch_X:
                        domains: no_arch
                        supported:
                            arches: X
                    no_arch_domain_arch_Y:
                        domains: no_arch
                        supported:
                            arches: Y
                    no_arch_domain_all_arches:
                        domains: no_arch
                        supported:
                            arches: .*

                    arch_X_domain_no_arches:
                        domains: arch_X
                    arch_X_domain_arch_X:
                        domains: arch_X
                        supported:
                            arches: X
                    arch_X_domain_arch_Y:
                        domains: arch_X
                        supported:
                            arches: Y
                    arch_X_domain_all_arches:
                        domains: arch_X
                        supported:
                            arches: .*

                    arch_Y_domain_no_arches:
                        domains: arch_Y
                    arch_Y_domain_arch_X:
                        domains: arch_Y
                        supported:
                            arches: X
                    arch_Y_domain_arch_Y:
                        domains: arch_Y
                        supported:
                            arches: Y
                    arch_Y_domain_all_arches:
                        domains: arch_Y
                        supported:
                            arches: .*
                arches:
                    - X
                    - Y
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        all_domains_no_arches:
                            name: all_domains_no_arches
                            host_types: all_domains_no_arches
                        all_domains_arch_X:
                            name: all_domains_arch_X
                            host_types: all_domains_arch_X
                        all_domains_arch_Y:
                            name: all_domains_arch_Y
                            host_types: all_domains_arch_Y
                        all_domains_all_arches:
                            name: all_domains_all_arches
                            host_types: all_domains_all_arches

                        no_arch_domain_no_arches:
                            name: no_arch_domain_no_arches
                            host_types: no_arch_domain_no_arches
                        no_arch_domain_arch_X:
                            name: no_arch_domain_arch_X
                            host_types: no_arch_domain_arch_X
                        no_arch_domain_arch_Y:
                            name: no_arch_domain_arch_Y
                            host_types: no_arch_domain_arch_Y
                        no_arch_domain_all_arches:
                            name: no_arch_domain_all_arches
                            host_types: no_arch_domain_all_arches

                        arch_X_domain_no_arches:
                            name: arch_X_domain_no_arches
                            host_types: arch_X_domain_no_arches
                        arch_X_domain_arch_X:
                            name: arch_X_domain_arch_X
                            host_types: arch_X_domain_arch_X
                        arch_X_domain_arch_Y:
                            name: arch_X_domain_arch_Y
                            host_types: arch_X_domain_arch_Y
                        arch_X_domain_all_arches:
                            name: arch_X_domain_all_arches
                            host_types: arch_X_domain_all_arches

                        arch_Y_domain_no_arches:
                            name: arch_Y_domain_no_arches
                            host_types: arch_Y_domain_no_arches
                        arch_Y_domain_arch_X:
                            name: arch_Y_domain_arch_X
                            host_types: arch_Y_domain_arch_X
                        arch_Y_domain_arch_Y:
                            name: arch_Y_domain_arch_Y
                            host_types: arch_Y_domain_arch_Y
                        arch_Y_domain_all_arches:
                            name: arch_Y_domain_all_arches
                            host_types: arch_Y_domain_all_arches
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        domains_arches_tests = {
            ".*": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_arch_Y
                    arch_X_domain_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_X
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_no_arches
                """.split(),
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
            },
            "no_arch": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_X
                    no_arch_domain_no_arches
                """.split(),
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    no_arch_domain_all_arches
                    no_arch_domain_arch_Y
                    no_arch_domain_no_arches
                """.split(),
            },
            "arch_X": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_arch_Y
                    arch_X_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                """.split(),
                "X": """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_no_arches
                    arch_X_domain_all_arches
                    arch_X_domain_arch_X
                    arch_X_domain_no_arches
                """.split(),
                "Y": "",
            },
            "arch_Y": {
                None: """
                    all_domains_all_arches
                    all_domains_arch_X
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_X
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
                ".*": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
                "X": "",
                "Y": """
                    all_domains_all_arches
                    all_domains_arch_Y
                    all_domains_no_arches
                    arch_Y_domain_all_arches
                    arch_Y_domain_arch_Y
                    arch_Y_domain_no_arches
                """.split(),
            },
        }
        with assets_mkdir(assets) as db_path:
            for domains, arches_tests in domains_arches_tests.items():
                for arch, tests in arches_tests.items():
                    args = [kpet_test_list, db_path, "--domains", domains]
                    if arch is not None:
                        args += ["--arch", arch]
                    self.assertKpetProduces(
                        *args,
                        stdout_equals="".join(
                            f'{test}\n'
                            for test in tests
                        ),
                        name=f"Listing tests for {domains!r} domains "
                        f"and {arch!r} arch"
                    )

    def test_nested_domain_hostname_inheritance(self):
        """
        Check nested domains inherit forced hostnames from host types
        correctly.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        domains:
                            X1:
                                description: Hosts X1
                            X2:
                                description: Hosts X2
                                domains:
                                    X21:
                                        description: Hosts X21
                    Y:
                        description: Hosts Y
                host_types:
                    x:
                        domains: X
                    x1:
                        domains: X1
                        hostname: foo
                    x2:
                        domains: X2
                    x21:
                        domains: X21
                        hostname: bar
                    y:
                        domains: Y
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testX:
                            name: TestX
                            host_types: x
                        testX1:
                            name: TestX1
                            host_types: x1
                        testX2:
                            name: TestX2
                            host_types: x2
                        testX21:
                            name: TestX21
                            host_types: x21
                        testY:
                            name: TestY
                            host_types: y
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<>:TEST:TestX'
                              'RECIPESET:'
                              'HOST<!H:bar&H:foo>:TEST:TestX1'
                              'RECIPESET:'
                              'HOST<!H:foo>:TEST:TestX2'
                              'RECIPESET:'
                              'HOST<H:bar&!H:foo>:TEST:TestX21'
                              'RECIPESET:'
                              'HOST<!H:bar&!H:foo>:TEST:TestY'
            )

    def test_nested_domain_selection(self):
        """
        Check nested domain selection works correctly. Including:
        * tests from an unselected superdomain run in a selected subdomain,
        * selecting a domain doesn't select parent domains,
        * selecting a domain selects subdomains,
        * selecting a domain doesn't select other domains.
        * most specific domain is picked out of multiple selected matches
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        domains:
                            X1:
                                description: Hosts X1
                            X2:
                                description: Hosts X2
                                domains:
                                    X21:
                                        description: Hosts X21
                    Y:
                        description: Hosts Y
                host_types:
                    x:
                        domains: X
                    x1:
                        domains: X1
                        hostname: foo
                    x2:
                        domains: X2
                    x21:
                        domains: X21
                        hostname: bar
                    y:
                        domains: Y
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testX:
                            name: TestX
                            host_types: x
                        testX1:
                            name: TestX1
                            host_types: x1
                        testX2:
                            name: TestX2
                            host_types: x2
                        testX21:
                            name: TestX21
                            host_types: x21
                        testY:
                            name: TestY
                            host_types: y
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X2",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!H:foo>:TEST:TestX'
                              'RECIPESET:'
                              'HOST<!H:foo>:TEST:TestX2'
                              'RECIPESET:'
                              'HOST<H:bar&!H:foo>:TEST:TestX21'
            )

        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: X
                        domains:
                            X1:
                                description: Hosts X1
                                host_requires: X1
                            X2:
                                description: Hosts X2
                                host_requires: X2
                                domains:
                                    X21:
                                        description: Hosts X21
                                        host_requires: X21
                    Y:
                        description: Hosts Y
                host_types:
                    x:
                        domains: X2.*
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testX:
                            name: TestX
                            host_types: x
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<T:X&!T:X1&T:X2>:TEST:TestX'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X2",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<T:X&!T:X1&T:X2>:TEST:TestX'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--domains", "X21",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST<T:X&!T:X1&T:X2&T:X21>:TEST:TestX'
            )

    def test_requires_rejects_propagate_to_subdomains(self):
        """
        Check that requires and rejects are propagated to host types attached
        to subdomains.
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: X
                        domains:
                            X1:
                                description: Hosts X1
                                host_requires: X1
                                domains:
                                    X11:
                                        description: Hosts X11
                                        host_requires: X11
                    Y:
                        description: Hosts Y
                        host_requires: Y
                        domains:
                            Y1:
                                description: Hosts Y1
                                host_requires: true
                                domains:
                                    Y11:
                                        description: Hosts Y11
                                        host_requires: Y11
                    Z:
                        description: Hosts Z
                        host_requires: Z
                        domains:
                            Z1:
                                description: Hosts Z1
                                host_requires: false
                                domains:
                                    Z11:
                                        description: Hosts Z11
                                        host_requires: Z11
                    N:
                        description: Hosts N
                        host_requires: N
                        domains:
                            N1:
                                description: Hosts N1
                                domains:
                                    N11:
                                        description: Hosts N11
                                        host_requires: N11
                host_types:
                    x11:
                        domains: X11
                    y11:
                        domains: Y11
                    z11:
                        domains: Z11
                    n11:
                        domains: N11
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testX11:
                            name: TestX11
                            host_types: x11
                        testY11:
                            name: TestY11
                            host_types: y11
                        testZ11:
                            name: TestZ11
                            host_types: z11
                        testN11:
                            name: TestN11
                            host_types: n11
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!T:N&T:X&T:X1&T:X11&!T:Y&!T:Z>:'
                              'TEST:TestX11'
                              'RECIPESET:'
                              'HOST<!T:N&!T:X&T:Y&T:Y11&!T:Z>:TEST:TestY11'
                              'RECIPESET:'
                              'HOST<!T:N&!T:X&!T:Y&T:Z&T:Z11>:TEST:TestZ11'
                              'RECIPESET:'
                              'HOST<T:N&T:N11&!T:X&!T:Y&!T:Z>:TEST:TestN11'
            )

    def test_requires_rejects_dont_propagate_to_superdomains(self):
        """
        Check that requires and rejects don't propagate to parent domains
        (superdomains).
        """
        assets = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                        host_requires: X
                        domains:
                            X1:
                                description: Hosts X1
                                host_requires: X1
                                domains:
                                    X11:
                                        description: Hosts X11
                                        host_requires: X11
                                    X12:
                                        description: Hosts X12
                                        host_requires: X12
                    Y:
                        description: Hosts Y
                        host_requires: Y
                        domains:
                            Y1:
                                description: Hosts Y1
                                host_requires: true
                                domains:
                                    Y11:
                                        description: Hosts Y11
                                        host_requires: Y11
                                    Y12:
                                        description: Hosts Y12
                                        host_requires: Y12
                    Z:
                        description: Hosts Z
                        host_requires: Z
                        domains:
                            Z1:
                                description: Hosts Z1
                                host_requires: false
                                domains:
                                    Z11:
                                        description: Hosts Z11
                                        host_requires: Z11
                                    Z12:
                                        description: Hosts Z12
                                        host_requires: Z12
                    N:
                        description: Hosts N
                        host_requires: N
                        domains:
                            N1:
                                description: Hosts N1
                                domains:
                                    N11:
                                        description: Hosts N11
                                        host_requires: N11
                                    N12:
                                        description: Hosts N12
                                        host_requires: N12
                host_types:
                    x:
                        domains: X
                    y:
                        domains: Y
                    z:
                        domains: Z
                    n:
                        domains: N
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testX:
                            name: TestX
                            host_types: x
                        testY:
                            name: TestY
                            host_types: y
                        testZ:
                            name: TestZ
                            host_types: z
                        testN:
                            name: TestN
                            host_types: n
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!T:N&T:X&!T:Y&!T:Z>:TEST:TestX'
                              'RECIPESET:'
                              'HOST<!T:N&!T:X&T:Y&!T:Z>:TEST:TestY'
                              'RECIPESET:'
                              'HOST<!T:N&!T:X&!T:Y&T:Z>:TEST:TestZ'
                              'RECIPESET:'
                              'HOST<T:N&!T:X&!T:Y&!T:Z>:TEST:TestN'
            )

    def test_child_requires_propagate_to_rejects(self):
        """
        Check that rejects are generated from child requires,
        if domain requires is missing or False.
        """
        assets = {
            "index.yaml": """
                domains:
                    A:
                        description: Hosts A
                        domains:
                            A1:
                                description: Hosts A1
                                host_requires: A1
                    B:
                        description: Hosts B
                        domains:
                            B1:
                                description: Hosts B1
                                host_requires: B1
                            B2:
                                description: Hosts B2
                                host_requires: B2
                    C:
                        description: Hosts C
                        host_requires: false
                        domains:
                            C1:
                                description: Hosts C1
                                host_requires: C1
                    D:
                        description: Hosts D
                        host_requires: false
                        domains:
                            D1:
                                description: Hosts D1
                                host_requires: D1
                            D2:
                                description: Hosts D2
                                host_requires: D2
                host_types:
                    a:
                        domains: A
                    b:
                        domains: B
                    c:
                        domains: C
                    d:
                        domains: D
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testA:
                            name: TestA
                            host_types: a
                        testB:
                            name: TestB
                            host_types: b
                        testC:
                            name: TestC
                            host_types: c
                        testD:
                            name: TestD
                            host_types: d
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:A1&!T:B1&!T:B2&'
                              '!T:C1&!T:D1&!T:D2>:TEST:TestA'
                              'RECIPESET:'
                              'HOST<!T:A1&!T:C1&!T:D1&!T:D2&'
                              '(T:B1|T:B2)>:TEST:TestB'
                              'RECIPESET:'
                              'HOST<!T:A1&!T:B1&!T:B2&'
                              'T:C1&!T:D1&!T:D2>:TEST:TestC'
                              'RECIPESET:'
                              'HOST<!T:A1&!T:B1&!T:B2&!T:C1&'
                              '(T:D2|T:D1)>:TEST:TestD'
            )

    def test_child_requires_dont_propagate_to_rejects(self):
        """
        Check that rejects are *not* generated from child requires,
        if domain requires is True.
        """
        assets = {
            "index.yaml": """
                domains:
                    A:
                        description: Hosts A
                        host_requires: true
                        domains:
                            A1:
                                description: Hosts A1
                                host_requires: A1
                    B:
                        description: Hosts B
                        domains:
                            B1:
                                description: Hosts B1
                                host_requires: B1
                    C:
                        description: Hosts C
                        host_requires: false
                        domains:
                            C1:
                                description: Hosts C1
                                host_requires: C1
                            C2:
                                description: Hosts C2
                                host_requires: C2
                host_types:
                    a:
                        domains: A
                    b:
                        domains: B
                    c:
                        domains: C
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testA:
                            name: TestA
                            host_types: a
                        testB:
                            name: TestB
                            host_types: b
                        testC:
                            name: TestC
                            host_types: c
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!T:B1&!T:C1&!T:C2>:TEST:TestA'
                              'RECIPESET:'
                              'HOST<T:B1&!T:C1&!T:C2>:TEST:TestB'
                              'RECIPESET:'
                              'HOST<!T:B1&(T:C1|T:C2)>:TEST:TestC'
            )

        assets = {
            "index.yaml": """
                domains:
                    A:
                        description: Hosts A
                        host_requires: true
                        domains:
                            A1:
                                description: Hosts A1
                                host_requires: A1
                            A2:
                                description: Hosts A2
                                host_requires: A2
                    B:
                        description: Hosts B
                        domains:
                            B1:
                                description: Hosts B1
                                host_requires: B1
                    C:
                        description: Hosts C
                        host_requires: false
                        domains:
                            C1:
                                description: Hosts C1
                                host_requires: C1
                            C2:
                                description: Hosts C2
                                host_requires: C2
                host_types:
                    a:
                        domains: A
                    b:
                        domains: B
                    c:
                        domains: C
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        testA:
                            name: TestA
                            host_types: a
                        testB:
                            name: TestB
                            host_types: b
                        testC:
                            name: TestC
                            host_types: c
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!T:B1&!T:C1&!T:C2>:TEST:TestA'
                              'RECIPESET:'
                              'HOST<T:B1&!T:C1&!T:C2>:TEST:TestB'
                              'RECIPESET:'
                              'HOST<!T:B1&(T:C1|T:C2)>:TEST:TestC'
            )

    def test_ampere(self):
        """
        Check that Ampere Altra usecase is supported. That is:
        * We can have a normal run, where the Ampere hosts would be used
          equally with all other hosts.
        * We can have a special run with two executions of all the tests:
            * On all hosts except Ampere hosts;
            * On Ampere hosts.
        * In both cases the separate, dedicated domains are used too.
        """
        assets = {
            "index.yaml": """
                domains:
                    shared:
                        description: Hosts shared by most tests
                        host_requires: shared
                        domains:
                            general:
                                description: General-purpose hosts
                                host_requires: general
                            ampere:
                                description: Ampere Altra hosts
                                host_requires: ampere
                    dedicated:
                        description: Hosts dedicated to exclusive testing
                        host_requires: dedicated
                        domains:
                            rdma:
                                description: Hosts for RDMA testing
                                host_requires: rdma
                            watchdog:
                                description: Hosts for watchdog testing
                                host_requires: watchdog
                host_types:
                    any:
                        domains: .*
                    shared:
                        domains: shared
                    general:
                        domains: general
                    ampere:
                        domains: ampere
                    dedicated:
                        domains: dedicated
                    rdma:
                        domains: rdma
                    watchdog:
                        domains: watchdog
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        any:
                            name: any
                            host_types: any
                        shared:
                            name: shared
                            host_types: shared
                        general:
                            name: general
                            host_types: general
                        ampere:
                            name: ampere
                            host_types: ampere
                        dedicated:
                            name: dedicated
                            host_types: dedicated
                        rdma:
                            name: rdma
                            host_types: rdma
                        watchdog:
                            name: watchdog
                            host_types: watchdog
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            # Normal run
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<!T:dedicated&T:shared>:TEST:any'
                              'RECIPESET:'
                              'HOST<!T:dedicated&T:shared>:TEST:shared'
                              'RECIPESET:'
                              'HOST<!T:ampere&!T:dedicated&'
                              'T:general&T:shared>:TEST:general'
                              'RECIPESET:'
                              'HOST<T:ampere&!T:dedicated&'
                              '!T:general&T:shared>:TEST:ampere'
                              'RECIPESET:'
                              'HOST<T:dedicated&!T:shared>:TEST:dedicated'
                              'RECIPESET:'
                              'HOST<T:dedicated&T:rdma&'
                              '!T:shared&!T:watchdog>:TEST:rdma'
                              'RECIPESET:'
                              'HOST<T:dedicated&!T:rdma&'
                              '!T:shared&T:watchdog>:TEST:watchdog'
            )
            # Special run
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                '--domains=general|dedicated', '-e', '--domains=ampere',
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:dedicated&!T:shared>:TEST:any'
                              'RECIPESET:'
                              'HOST<!T:ampere&!T:dedicated&'
                              'T:general&T:shared>:TEST:shared'
                              'RECIPESET:'
                              'HOST<!T:ampere&!T:dedicated&'
                              'T:general&T:shared>:TEST:general'
                              'RECIPESET:'
                              'HOST<T:dedicated&!T:shared>:TEST:dedicated'
                              'RECIPESET:'
                              'HOST<T:dedicated&T:rdma&'
                              '!T:shared&!T:watchdog>:TEST:rdma'
                              'RECIPESET:'
                              'HOST<T:dedicated&!T:rdma&'
                              '!T:shared&T:watchdog>:TEST:watchdog'
                              'SCENE:'
                              'RECIPESET:'
                              'HOST<T:ampere&!T:dedicated&'
                              '!T:general&T:shared>:TEST:any'
                              'RECIPESET:'
                              'HOST<T:ampere&!T:dedicated&'
                              '!T:general&T:shared>:TEST:shared'
                              'RECIPESET:'
                              'HOST<T:ampere&!T:dedicated&'
                              '!T:general&T:shared>:TEST:ampere'
            )

    def test_component_host_requires(self):
        """
        Check that component host_requires are applied correctly
        """
        assets = {
            "index.yaml": """
                host_types:
                    a:
                        host_requires: a
                    b:
                        host_requires: b
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        a:
                            name: a
                            host_types: a
                        b:
                            name: b
                            host_types: b
                components:
                    x:
                        description: X component
                        host_requires: x
                    y:
                        description: Y component
                        host_requires: y
                    no_host_requires:
                        description: Component without host_requires
            """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                name="Run generation without components",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:a>:TEST:a'
                              'RECIPESET:'
                              'HOST<T:b>:TEST:b'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--components", "no_host_requires",
                name="Run generation with a component without host_requires",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:a>:TEST:a'
                              'RECIPESET:'
                              'HOST<T:b>:TEST:b'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--components", "x",
                name="Run generation with a component with host_requires",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:a&T:x>:TEST:a'
                              'RECIPESET:'
                              'HOST<T:b&T:x>:TEST:b'
            )
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "--components", "x|y",
                name="Run generation with two components with host_requires",
                stdout_equals='SCENE:'
                              'RECIPESET:'
                              'HOST<T:a&T:x&T:y>:TEST:a'
                              'RECIPESET:'
                              'HOST<T:b&T:x&T:y>:TEST:b'
            )
